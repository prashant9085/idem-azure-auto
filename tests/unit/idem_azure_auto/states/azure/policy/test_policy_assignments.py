import copy
from collections import ChainMap

import pytest

RESOURCE_NAME = "policy_assignment_test"
SCOPE = "/subscriptions/subscription_id/resourceGroups/my-resource-group"
POLICY_ASSIGNMENT_NAME = "policy_assignment_test"
POLICY_DEFINITION_ID = "/subscriptions/subscription_id/providers/Microsoft.Authorization/policyDefinitions/idem-test-location-policy-definition-1657624239"
POLICY_ASSIGNMENT_PARAMETERS = {
    "prefix": {"value": "DeptA"},
    "suffix": {"value": "-LC"},
}
RESOURCE_PARAMETERS = {
    "policy_assignment_name": POLICY_ASSIGNMENT_NAME,
    "policy_definition_id": POLICY_DEFINITION_ID,
    "scope": SCOPE,
    "parameters": POLICY_ASSIGNMENT_PARAMETERS,
}

RESOURCE_PARAMETERS_RAW = {
    "properties": {
        "policyDefinitionId": "/subscriptions/subscription_id/providers/Microsoft.Authorization/policyDefinitions/idem-test-location-policy-definition-1657624239",
        "scope": SCOPE,
        "parameters": {"prefix": {"value": "DeptA"}, "suffix": {"value": "-LC"}},
    }
}


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of policy assignments. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.policy.policy_assignments.present = (
        hub.states.azure.policy.policy_assignments.present
    )
    mock_hub.tool.azure.policy.policy_assignment.convert_raw_policy_assignment_to_present = (
        hub.tool.azure.policy.policy_assignment.convert_raw_policy_assignment_to_present
    )
    mock_hub.tool.azure.policy.policy_assignment.convert_present_to_raw_policy_assignment = (
        hub.tool.azure.policy.policy_assignment.convert_present_to_raw_policy_assignment
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    expected_put = {
        "ret": {
            "id": f"{SCOPE}/providers/Microsoft.Authorization/policyAssignments/{POLICY_ASSIGNMENT_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": f"Would create azure.policy.policy_assignments {RESOURCE_NAME}",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert POLICY_ASSIGNMENT_NAME in url
        assert SCOPE in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert POLICY_ASSIGNMENT_NAME in url
        assert SCOPE in url
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.policy.policy_assignments.present(
        test_ctx,
        RESOURCE_NAME,
        SCOPE,
        POLICY_ASSIGNMENT_NAME,
        POLICY_DEFINITION_ID,
        POLICY_ASSIGNMENT_PARAMETERS,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Would create azure.policy.policy_assignments '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters
    ret = await mock_hub.states.azure.policy.policy_assignments.present(
        ctx,
        RESOURCE_NAME,
        SCOPE,
        POLICY_ASSIGNMENT_NAME,
        POLICY_DEFINITION_ID,
        POLICY_ASSIGNMENT_PARAMETERS,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Created azure.policy.policy_assignments '{RESOURCE_NAME}'" in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of policy assignments. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """
    mock_hub.states.azure.policy.policy_assignments.present = (
        hub.states.azure.policy.policy_assignments.present
    )
    mock_hub.tool.azure.policy.policy_assignment.convert_raw_policy_assignment_to_present = (
        hub.tool.azure.policy.policy_assignment.convert_raw_policy_assignment_to_present
    )
    mock_hub.tool.azure.policy.policy_assignment.convert_present_to_raw_policy_assignment = (
        hub.tool.azure.policy.policy_assignment.convert_present_to_raw_policy_assignment
    )
    mock_hub.tool.azure.policy.policy_assignment.update_policy_assignment_payload = (
        hub.tool.azure.policy.policy_assignment.update_policy_assignment_payload
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    expected_get = {
        "ret": {
            "id": f"{SCOPE}/providers/Microsoft.Authorization/policyAssignments/{POLICY_ASSIGNMENT_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    updated_params = {"prefix": {"value": "DeptB"}, "suffix": {"value": "-LC"}}
    resource_updated_raw = {
        "properties": {
            "policyDefinitionId": POLICY_DEFINITION_ID,
            "scope": SCOPE,
            "parameters": updated_params,
        }
    }
    expected_updated_parameters = {
        "policy_assignment_name": POLICY_ASSIGNMENT_NAME,
        "policy_definition_id": POLICY_DEFINITION_ID,
        "scope": SCOPE,
        "parameters": updated_params,
    }
    expected_put = {
        "ret": {
            "id": f"{SCOPE}/providers/Microsoft.Authorization/policyAssignments/{POLICY_ASSIGNMENT_NAME}",
            "name": RESOURCE_NAME,
            **resource_updated_raw,
        },
        "result": True,
        "status": 200,
        "comment": f"Would update azure.policy.policy_assignments with parameters: {RESOURCE_NAME}.",
    }

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert POLICY_ASSIGNMENT_NAME in url
        assert SCOPE in url
        return expected_put

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.policy.policy_assignments.present(
        test_ctx,
        RESOURCE_NAME,
        SCOPE,
        POLICY_ASSIGNMENT_NAME,
        POLICY_DEFINITION_ID,
        updated_params,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Would update azure.policy.policy_assignments '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=expected_updated_parameters,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters
    ret = await mock_hub.states.azure.policy.policy_assignments.present(
        ctx,
        RESOURCE_NAME,
        SCOPE,
        POLICY_ASSIGNMENT_NAME,
        POLICY_DEFINITION_ID,
        updated_params,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Updated azure.policy.policy_assignments '{RESOURCE_NAME}'" in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=expected_updated_parameters,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of policy assignments. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.policy.policy_assignments.absent = (
        hub.states.azure.policy.policy_assignments.absent
    )
    mock_hub.tool.azure.policy.policy_assignment.convert_raw_policy_assignment_to_present = (
        hub.tool.azure.policy.policy_assignment.convert_raw_policy_assignment_to_present
    )
    mock_hub.tool.azure.policy.policy_assignment.convert_present_to_raw_policy_assignment = (
        hub.tool.azure.policy.policy_assignment.convert_present_to_raw_policy_assignment
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert POLICY_ASSIGNMENT_NAME in url
        assert SCOPE in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.policy.policy_assignments.absent(
        ctx,
        RESOURCE_NAME,
        SCOPE,
        POLICY_ASSIGNMENT_NAME,
    )
    assert not ret["changes"]
    assert RESOURCE_NAME == ret.get("name")
    assert ret["result"]
    assert (
        f"azure.policy.policy_assignments '{RESOURCE_NAME}' already absent"
        in ret.get("comment")
    )


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of policy assignments. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.policy.policy_assignments.absent = (
        hub.states.azure.policy.policy_assignments.absent
    )
    mock_hub.tool.azure.policy.policy_assignment.convert_raw_policy_assignment_to_present = (
        hub.tool.azure.policy.policy_assignment.convert_raw_policy_assignment_to_present
    )
    mock_hub.tool.azure.policy.policy_assignment.convert_present_to_raw_policy_assignment = (
        hub.tool.azure.policy.policy_assignment.convert_present_to_raw_policy_assignment
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    expected_get = {
        "ret": {
            "id": f"{SCOPE}/providers/Microsoft.Authorization/policyAssignments/{POLICY_ASSIGNMENT_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": f"Deleted azure.policy.policy_assignments '{RESOURCE_NAME}'",
    }

    def _check_delete_parameters(_ctx, url, success_codes):
        assert POLICY_ASSIGNMENT_NAME in url
        assert SCOPE in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test absent() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.policy.policy_assignments.absent(
        test_ctx,
        RESOURCE_NAME,
        SCOPE,
        POLICY_ASSIGNMENT_NAME,
    )
    assert RESOURCE_NAME == ret.get("name")
    assert ret["result"]
    assert (
        f"Would delete azure.policy.policy_assignments '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )

    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters
    # Test absent() with --test flag off
    ret = await mock_hub.states.azure.policy.policy_assignments.absent(
        ctx,
        RESOURCE_NAME,
        SCOPE,
        POLICY_ASSIGNMENT_NAME,
    )
    assert RESOURCE_NAME == ret.get("name")
    assert ret["result"]
    assert expected_delete.get("comment") in ret.get("comment")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of policy assignments.
    """
    mock_hub.states.azure.policy.policy_assignments.describe = (
        hub.states.azure.policy.policy_assignments.describe
    )
    mock_hub.tool.azure.policy.policy_assignment.convert_raw_policy_assignment_to_present = (
        hub.tool.azure.policy.policy_assignment.convert_raw_policy_assignment_to_present
    )
    mock_hub.tool.azure.policy.policy_assignment.convert_present_to_raw_policy_assignment = (
        hub.tool.azure.policy.policy_assignment.convert_present_to_raw_policy_assignment
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value = hub.tool.azure.uri.get_parameter_value
    resource_id = f"{SCOPE}/providers/Microsoft.Authorization/policyAssignments/{POLICY_ASSIGNMENT_NAME}"
    expected_list = {
        "ret": {
            "value": [
                {"id": resource_id, "name": RESOURCE_NAME, **RESOURCE_PARAMETERS_RAW}
            ]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = await mock_hub.states.azure.policy.policy_assignments.describe(ctx)

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert "azure.policy.policy_assignments.present" in ret_value.keys()
    described_resource = ret_value.get("azure.policy.policy_assignments.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert resource_id == described_resource_map.get("name")
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


def check_returned_states(old_state, new_state, expected_old_state, expected_new_state):
    if old_state:
        assert POLICY_ASSIGNMENT_NAME == old_state.get("policy_assignment_name")
        assert expected_old_state["scope"] == old_state.get("scope")
        assert expected_old_state["policy_definition_id"] == old_state.get(
            "policy_definition_id"
        )
        assert expected_old_state["parameters"] == old_state.get("parameters")
    if new_state:
        assert POLICY_ASSIGNMENT_NAME == new_state.get("policy_assignment_name")
        assert expected_new_state["scope"] == new_state.get("scope")
        assert expected_new_state["policy_definition_id"] == new_state.get(
            "policy_definition_id"
        )
        assert expected_new_state["parameters"] == new_state.get("parameters")
