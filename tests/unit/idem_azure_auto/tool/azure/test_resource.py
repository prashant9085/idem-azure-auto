def test_check_response_payload(hub, mock_hub):
    expected_payload = {
        "location": "eastus",
        "properties": {
            "addressSpace": {"addressPrefixes": ["10.12.13.0/25"]},
            "flowTimeoutInMinutes": 10,
        },
    }
    real_payload = {
        "name": "test-vnet",
        "id": "/subscriptions/subid/resourceGroups/rg1/providers/Microsoft.Network/virtualNetworks/test-vnet",
        "type": "Microsoft.Network/virtualNetworks",
        "location": "eastus",
        "properties": {
            "provisioningState": "Succeeded",
            "addressSpace": {"addressPrefixes": ["10.12.13.0/25"]},
            "flowTimeoutInMinutes": 10,
            "subnets": [],
            "virtualNetworkPeerings": [],
        },
    }
    mock_hub.tool.azure.resource.check_response_payload = (
        hub.tool.azure.resource.check_response_payload
    )

    mock_hub.tool.azure.resource.check_response_payload(expected_payload, real_payload)
