=============================
Idem-azure-auto Release 0.0.2
=============================

This release includes Idem state support on more azure resources.
The Idem version is bumped to Idem 14. Idem-aiohttp version is bumped to 3.0.0.
The version bump of these two dependencies allow Idem-azure-auto to more strictly
comply with Idem's plugin requirement.

The new Idem state support includes:
authorization service: role_assignments, role_definitions
storage resource provider service: storage_accounts
virtual network service: nat_gateways, route_tables, routes
